'use strict'

const { VueLoaderPlugin } = require('vue-loader')
const path = require('path');


module.exports = {
  mode: 'development',
  entry: [
    './src/app.js'
  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.css$/,
        use: [{
          loader: "style-loader"
          },
          {
          loader: "css-loader"
          }]
      }
    ]
  },
  devtool: '#source-map',
  output: {
      publicPath: '/dist/',
      filename: '[name].js',
      chunkFilename: '[name].chunk.js'
  },
  plugins: [
    new VueLoaderPlugin()
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9000,
    historyApiFallback: true,
    hot: true,
    inline: true,
    stats: { colors: true },
    proxy: {
      //匹配代理的url
      '/api': {
      // 目标服务器地址
        target: 'http://127.0.0.1:8899',
        //路径重写
        pathRewrite: {'^/api' : '/'},
        changeOrigin: true
      }
    }
  }
}