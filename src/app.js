import Vue from 'vue'
import App from './App.vue'
import Routers from './router/router';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const RouterConfig = {
    mode: 'history',
    routes: Routers
};
const router = new VueRouter(RouterConfig);

new Vue({
    el: '#app',
    router: router,
    render: h => h(App),
    created: function () {
        // `this` points to the vm instance
        console.log('app created.')
      }
})