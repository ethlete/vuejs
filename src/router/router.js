import WhatsVue from './../pages/WhatsVue.vue';
import DeclaritiveRender from './../pages/DeclaritiveRender.vue';
import Conditionals from './../pages/ConditionalsLoops.vue'
import HandlingUserInput from './../pages/HandlingUserInput.vue'
import ComposingWithComponents from './../pages/ComposingWithComponents.vue'
import TemplateSyntax from './../pages/TemplateSyntax.vue'
import ComputedPropsWatchers from './../pages/ComputedPropsWatchers.vue'
import ClassStyle from './../pages/ClassStyle.vue'
import ConditionalRendering from './../pages/ConditionalRendering.vue'
import ListRendering from './../pages/ListRendering.vue'
import FormInputBindings from './../pages/FormInputBindings.vue'

const routers = [
    {
        path: '/whats-vue',
        meta: {
            title: 'What\'s Vue.js'
        },
        component: WhatsVue
    },
    {
        path: '/declaritive-render',
        meta: {
            title: 'Declarative Rendering'
        },
        component: DeclaritiveRender
    },
    {
        path: '/condition-loops',
        meta: {
            title: 'Conditionals and Loops'
        },
        component: Conditionals
    },
    {
        path: '/handling-user-input',
        meta: {
            title: 'Handling User Input'
        },
        component: HandlingUserInput
    },
    // Composing with Components
    {
        path: '/composing-with-components',
        meta: {
            title: 'Composing with Components'
        },
        component: ComposingWithComponents
    },
    {
        path: '/template-syntax',
        meta: {
            title: 'Template Syntax'
        },
        component: TemplateSyntax
    },
    {
        path: '/computed-pros-watchers',
        meta: {
            title: 'Computed Properties and Watchers'
        },
        component: ComputedPropsWatchers
    },
    {
        path: '/class-style',
        meta: {
            title: 'Class and Style Bindings'
        },
        component: ClassStyle
    },
    {
        path: '/conditional-rendering',
        meta: {
            title: 'Conditional Rendering'
        },
        component: ConditionalRendering
    },
    {
        path: '/list-rendering',
        meta: {
            title: 'List Rendering'
        },
        component: ListRendering
    },
    {
        path: '/form-input-bindings',
        meta: {
            title: 'Form Input Bindings'
        },
        component: FormInputBindings
    },

    //  
];

export default routers;
